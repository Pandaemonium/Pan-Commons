package dev.pandaemonium.commons.core;

import dev.pandaemonium.commons.resource.PanResource;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Pandaemonium {

    private static final @NotNull Pandaemonium instance = new Pandaemonium();

    private final @NotNull HashMap<String, PanResource> resources = new HashMap<>();

    public static @NotNull Pandaemonium getInstance() {
        return instance;
    }

    public Pandaemonium() {

    }

    public void register(@NotNull PanResource resource) {
        if (resources.containsValue(resource)) return;
        resources.put(resource.getID(), resource);
    }

    public void unregister(@NotNull PanResource resource) {
        resources.remove(resource.getID());
    }

    public boolean isRegistered(@NotNull PanResource resource) {
        return resources.containsValue(resource);
    }

    public boolean isRegistered(@NotNull String id) {
        return resources.containsKey(id);
    }

    public PanResource getResource(@NotNull String id) {
        return resources.get(id);
    }

    public @NotNull List<PanResource> getResources() {
        return new ArrayList<>(resources.values());
    }

    public int getResourceCount() {
        return resources.size();
    }

    public int getModuleCount() {
        int i = 0;
        for (PanResource value : resources.values()) {
            i += value.getModules().size();
        }
        return i;
    }

    public String getVersion() {
        return "1.0.0";
    }

}
