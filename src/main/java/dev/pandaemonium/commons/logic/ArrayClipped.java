package dev.pandaemonium.commons.logic;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;

@SuppressWarnings("unused")
public interface ArrayClipped {

    /**
     * Serialises this object into a byte array
     * for easy and efficient storage.
     *
     * @return The data serialised to a byte array.
     * */
    @NotNull
    byte[] serialise();

    /**
     * Returns an encoded short describing the decimal places
     * of the input coordinate set.
     *
     * @param x The x-coordinate the first decimal place of which is to be encoded into the short
     * @param y The y-coordinate the first decimal place of which is to be encoded into the short
     * @param z The z-coordinate the first decimal place of which is to be encoded into the short
     *
     * @return The short describing the first decimal place of each input coordinate.
     * */
    static short decimalFromCoordinates(float x, float y, float z) {
        // This confusing mess gets the first decimal of each float
        int xDec = (int) (10*(new BigDecimal(String.valueOf(x)).setScale(1, RoundingMode.HALF_UP).doubleValue() - Math.floor(x)));
        int yDec = (int) (10*(new BigDecimal(String.valueOf(y)).setScale(1, RoundingMode.HALF_UP).doubleValue() - Math.floor(y)));
        int zDec = (int) (10*(new BigDecimal(String.valueOf(z)).setScale(1, RoundingMode.HALF_UP).doubleValue() - Math.floor(z)));

        String decimalBString = "+";
        decimalBString += binaryString(xDec, 4, false);
        decimalBString += binaryString(yDec, 4, false);
        decimalBString += binaryString(zDec, 4, false);
        decimalBString += "0000";

        return Short.parseShort(decimalBString, 2);
    }

    /**
     * Converts an integer to a binary string, padding it with padTo zeroes on the side
     * specified by padAfter.
     *
     * @param decimalValue The decimal integer to convert
     * @param padTo        The size the binary string should be padded to
     * @param padAfter     Whether padding should be applied to the front or end of the string.
     * @return A string representation of the decimalValue parameter's binary counterpart
     * */
    @NotNull
    static String binaryString(int decimalValue, int padTo, boolean padAfter) {
        String bin = Integer.toBinaryString(decimalValue);
        int pad = padTo-bin.length();
        if (pad <= 0) return bin;
        return (padAfter ? bin : "") + (new String(new char[pad]).replace("\0", "0")) + (padAfter ? "" : bin);
    }

}
