package dev.pandaemonium.commons.logic;

import java.io.File;
import java.util.HashSet;

@SuppressWarnings("unused")
public interface CachedStorage<T> {

    HashSet<T> getCache();

    File getStorageFile();

    default boolean merge() {
        return load(false) && save(true);
    }

    boolean save(boolean destructive);

    boolean load(boolean destructive);

}
