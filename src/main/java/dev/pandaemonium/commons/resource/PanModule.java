package dev.pandaemonium.commons.resource;

import org.jetbrains.annotations.NotNull;

public interface PanModule {

    @NotNull String getName();

    @NotNull String getID();

    @NotNull String getDescription();

    @NotNull PanResource getResource();

    void enable();

    void disable();

    boolean isEnabled();

}
