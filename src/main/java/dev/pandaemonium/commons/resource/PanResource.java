package dev.pandaemonium.commons.resource;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface PanResource {

    @NotNull String getName();

    @NotNull String getID();

    @NotNull String getResourceDescription();

    @NotNull String getVersion();

    @NotNull String getPanVersion();

    @NotNull List<PanModule> getModules();

}
